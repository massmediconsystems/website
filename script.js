// When the user scrolls the page, execute myFunction
window.onscroll = function () {
    stickyScroll();
  };

  var navbar = document.getElementById("sticky_nav");
  var sticky = navbar.offsetTop;

  function stickyScroll() {
    if (window.pageYOffset >= sticky) {
      navbar.classList.add("sticky");
    } else {
      navbar.classList.remove("sticky");
    }
  }

